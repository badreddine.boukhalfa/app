<?php namespace App\Entities;

use CodeIgniter\Entity;
use CodeIgniter\I18n\Time;

class Facture extends Entity
{

    public function setChrono()
    {
        $this->attributes['chrono'] = random_int(0,999999999);

        return $this;
    }

    public function getChrono()
    {
       
        return $this->attributes['chrono'];
    }

    public function setSent_at()
    {
        $this->attributes['sent_at'] =new time();

        return $this;
    }

    public function getSent_at(string $format = 'Y-m-d H:i:s')
    {
        // Convert to CodeIgniter\I18n\Time object
        $this->attributes['sent_at'] = $this->mutateDate($this->attributes['sent_at']);

        $timezone = $this->timezone ?? app_timezone();

        $this->attributes['sent_at']->setTimezone($timezone);

        return $this->attributes['sent_at']->format($format);
       
       
    }



}