
<body>
    <div class="container pt-5">
        <form method="POST" action="/client/edit/<?=$client['id']?>">
             <fieldset>
                <legend>Edit Client</legend>
            
                <div class="form-group">
                    <label for="exampleInputnom1">Nom</label>
                    <input type="text" class="form-control" id="exampleInputnom1" aria-describedby="nomHelp" placeholder="Nom" value ="<?=$client['nom']?>" name="nom">
                </div>
    
                <div class="form-group">
                    <label for="exampleInputprenom1">Prénom</label>
                    <input type="text" class="form-control" id="exampleInputprenom1" aria-describedby="prenomHelp" placeholder="Prenom" value ="<?=$client['prenom']?>" name="prenom">
                </div>
    
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value ="<?=$client['email']?>" name="email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
    
                <div class="form-group">
                    <label for="exampleInputEntreprise1">Entreprise</label>
                    <input type="text" class="form-control" id="exampleInputEntreprise1" aria-describedby="entrepriseHelp" placeholder="Entreprise" value ="<?=$client['entreprise']?>" name="entreprise">
                   
                </div>
            </fieldset>
        <div>
             <button type="submit" class="btn btn-primary">Update</button>
             <a href="/client"> retour à la list</a>
        </div>
          
    
        </form>
    </div>
</body>


</html>