
<!-- CONTENT -->
<body>
	
		<section class="container pt-5">

		<?php  $session=\config\Services::session(); ?>
		<?php if ( isset ($session->success)):?>
														
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				<strong> <?= $session->success ?> </strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

		<?php endif; ?>

		<div class="mb-3 d-flex justify-content-between align-items-center">
			<h1>Client</h1>
				<a href="/client/add">CREATE</a>
		</div>

		<table class="table table-hover">
		<thead>
			<tr>
			<th scope="col">id</th>
			<th scope="col">Nom</th>
			<th scope="col">Prenom</th>
			<th scope="col">email</th>
			<th scope="col">Entreprise</th>
			<th scope="col">Action</th>
			</tr>
		</thead>

		<tbody>
			<?php if ($clients): ?>
				<?php foreach($clients as $client): ?>
					<tr class="table-light">
						<th scope="row"><a href="client/edit/<?= $client->id ?>"><?= $client->id ?></a></th>
						<td><?= $client->nom ?></td>
						<td><?= $client->prenom ?></td>
						<td><?= $client->email ?></td>
						<td><?= $client->entreprise ?></td>
						<td>   <a href="/client/delete/<?= $client->id ?>" class="btn btn-outline-danger">supprimer</a></td>
					</tr>
				<?php endforeach; ?>	
			<?php else: ?>	
				<tr class="table-light">
					<td> pas de client</td>
				</tr>
			<?php endif; ?>	
		</tbody>
		</table>

		</section>

		<script>

		</script>

		<!-- -->

</body>
</html>
