
<body>
    <div class="container pt-5">
        <form method="POST" action="/client/add">
             <fieldset>
                <legend>Ajouter un nouveau Client</legend>
            
                <div class="form-group">
                    <label for="exampleInputnom1">Nom</label>
                    <input type="text" class="form-control" id="exampleInputnom1" aria-describedby="nomHelp" placeholder="Nom" name="nom">
                </div>
    
                <div class="form-group">
                    <label for="exampleInputprenom1">Prénom</label>
                    <input type="text" class="form-control" id="exampleInputprenom1" aria-describedby="prenomHelp" placeholder="Prenom" name="prenom">
                </div>
    
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
    
                <div class="form-group">
                    <label for="exampleInputEntreprise1">Entreprise</label>
                    <input type="text" class="form-control" id="exampleInputEntreprise1" aria-describedby="entrepriseHelp" placeholder="Entreprise" name="entreprise">
                   
                </div>
            </fieldset>
        <div>
             <button type="submit" class="btn btn-primary">Ajouter</button>
             <a href="/client"> retour à la list</a>
        </div>
          
    
        </form>
    </div>
</body>


</html>