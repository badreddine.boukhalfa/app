


<body>
    <div class="container pt-5">
        <form method="POST" action="/facture/add">
             <fieldset>
                <legend>Ajouter une Facture</legend>
            
                <div class="form-group">
                    <label for="exampleInputmontant1">montant</label>
                    <input type="number" class="form-control" id="exampleInputmontant1" aria-describedby="nomHelp" placeholder="amount" name="amount">
                </div>

                <div class="form-group">
                    <label for="exampleSelect1">Client</label>
                    <select class="form-control" id="exampleSelect1" name ="client_id">
                    <option value="">client</option>
                    <?php foreach($clients as $client):  ?>
                        <option value="<?= $client->id ?>"><?= $client->nom."-".$client->prenom ?></option>
                    <?php endforeach;  ?>   
                    </select>
                </div>

                <div class="form-group">
                    <label for="statusSelect1">status</label>
                    <select class="form-control" id="statusSelect1" name="status">
                        <option value="PAID">payée</option>
                        <option value="CANCELLED">annulée</option>
                        <option value="SENT">envoyée</option>
                    </select>
                </div>
    
            </fieldset>
        <div>
             <button type="submit" class="btn btn-primary">Ajouter</button>
             <a href="/facture"> retour à la list</a>
        </div>
          
    
        </form>
    </div>
</body>


</html>