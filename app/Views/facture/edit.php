<body>
    <div class="container pt-5">
        <form method="POST" action="/facture/edit/<?= $facture["chrono"] ?>">
             <fieldset>
                <legend>Ajouter une Facture</legend>
            
                <div class="form-group">
                    <label for="exampleInputmontant1">montant</label>
                    <input type="number" class="form-control" id="exampleInputmontant1" aria-describedby="nomHelp" placeholder="amount" name="amount" value="<?= $facture["amount"] ?>">
                </div>

                <div class="form-group">
                    <label for="exampleSelect1">Client</label>
                    <select class="form-control" id="exampleSelect1" name ="client_id">
                    <option value="">client</option>
                    <?php foreach($clients as $client):  ?>
                        <?php  if ($facture["client_id"]== $client->id ): ?>
                            <option value="<?= $client->id ?>" selected="selected"><?= $client->nom."-".$client->prenom ?></option>
                        <?php  else: ?>  
                            <option value="<?= $client->id ?>"><?= $client->nom."-".$client->prenom ?></option>
                        <?php  endif; ?>  
                        <?php endforeach;  ?>   
                    </select>
                </div>

                <div class="form-group">
                    <label for="statusSelect1">status</label>
                    <select class="form-control" id="statusSelect1" name="status">

                        <?php  if ($facture["status"]== "PAID" ): ?>
                                <option value="PAID" selected="selected"> payée</option>
                                <option value="CANCELLED">annulée</option>
                                <option value="SENT">envoyée</option>
                        <?php  endif; ?> 

                        <?php  if ($facture["status"]== "CANCELLED" ): ?>
                                <option value="PAID" > payée</option>
                                <option value="CANCELLED" selected="selected">annulée</option>
                                <option value="SENT">envoyée</option>
                        <?php  endif; ?> 

                        <?php  if ($facture["status"]== "SENT" ): ?>
                                <option value="PAID"> payée</option>
                                <option value="CANCELLED">annulée</option>
                                <option value="SENT" selected="selected">envoyée</option>
                        <?php  endif; ?> 
                    </select>
                </div>
    
            </fieldset>
        <div>
             <button type="submit" class="btn btn-primary">Ajouter</button>
             <a href="/facture"> retour à la list</a>
        </div>
          
    
        </form>
    </div>
</body>


</html>