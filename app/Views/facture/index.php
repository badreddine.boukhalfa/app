
<!-- CONTENT -->
<body>
<?php     
    
    $STATUS_LABELS = [
        'PAID'=>"Payée",
        'SENT'=>"Envoyée",
        'CANCELLED'=>"Annulée"];


        $STATUS_CLASSES = [
            "PAID"=>"success",
            "SENT"=> "primary",
            "CANCELLED"=>"danger"
        ];
    ?> 
		<section class="container pt-5">

		<?php  $session=\config\Services::session(); ?>
		<?php if ( isset ($session->success)):?>
														
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				<strong> <?= $session->success ?> </strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

		<?php endif; ?>

		<div class="mb-3 d-flex justify-content-between align-items-center">
			<h1>Factures</h1>
				<a href="/facture/add">CREATE</a>
		</div>

		<table class="table table-hover">
		<thead>
			<tr>
                <th scope="col">id</th>
                <th scope="col">Nom</th>
                <th scope="col">Prenom</th>
                <th scope="col">sentAt</th>
                <th scope="col">status</th>
                <th scope="col">amount</th>
                <th scope="col">action</th>
			</tr>
		</thead>
       
		<tbody>
			<?php if ($factures): ?>
                <?php foreach($factures as $facture): 
                    ?>
                    
					<tr class="table-light">
						<th scope="row"><a href="facture/edit/<?= $facture['chrono'] ?>"><?= $facture['chrono'] ?></a></th>
						<td> <?= $facture['nom']?></td>
						<td><?= $facture['prenom']?></td>
						<td><?= date('d/m/y',strtotime($facture['sent_at'])) ?></td>
                        <td> <span class="badge badge-<?=$STATUS_CLASSES[$facture['status']]?>" ><?= $STATUS_LABELS[$facture['status']] ?></span></td>
                        <td><?= $facture['amount'] ?> €</td>
						<td>   <a href="/facture/delete/<?= $facture['chrono']?>" class="btn btn-outline-danger">supprimer</a></td>
					</tr>
				<?php endforeach; ?>	
                <?php else: ?>	
                    <tr class="table-light">
                        <td> pas de facture</td>
                    </tr>
                <?php endif; ?>	
		</tbody>
		</table>

		</section>

		<script>

		</script>

		<!-- -->

</body>
</html>
