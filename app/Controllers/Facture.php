<?php 

namespace App\Controllers;
use App\Models\FactureModel;
use App\Models\ClientModel;
use App\Config\Service;

class Facture extends BaseController
{

	public function index()
	{
        $facture=  new FactureModel;
        $data['factures']=$facture->getFactureClient();
        echo view('common/header.php');
        return (view('facture/index.php',$data));
    }
    
    public function add()
	{
        $data = $this->request->getPost();
        $client=new ClientModel;
        $Allclient['clients']=$client->getClient();

        
        if ($data &&  $this->request->getMethod()=='post')
        {
            
            $FactureModel=  new FactureModel;
            $Factures = new \App\Entities\Facture($data);
            $Factures->setChrono();
            $Factures->setSent_at();
            if ($FactureModel->save($Factures))
            {
               $session=\config\Services::session();
               $session->setFlashdata('success','la Facture a été ajouter avec succée');
               return redirect()->to('/facture');
            }
            
        }
           echo view('common/header.php');
           echo view('facture/newFacture.php',$Allclient);
    }
    
 
    public function edit($chrono)
	{
        $facture=  new FactureModel;
        $data['facture']=$facture->getFactureByid($chrono);
        $client=new ClientModel;
        $data['clients']=$client->getClient();
        if ($this->request->getMethod()=='post')
        {
            $dataFacture = $this->request->getPost();
          
            if ($facture->update($data['facture']['id'], $dataFacture))
            {
                $session=\config\Services::session();
                $session->setFlashdata('success','la facture a été modifier avec succée');
                return redirect()->to('/facture');

            };
        }   
        echo view('common/header.php');
        return (view('facture/edit.php',$data));
	}

	//--------------------------------------------------------------------
    public function delete($id)
	{
        $facture=  new FactureModel;
        $facture->where('chrono',$id)->delete();
        $session=\config\Services::session();
        $session->setFlashdata('success','la facture a été supprimer avec succée');
        return redirect()->to('/facture');
   
    }
}
