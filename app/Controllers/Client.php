<?php 

namespace App\Controllers;
use App\Models\ClientModel;
use App\Config\Service;

class Client extends BaseController
{

	public function index()
	{
        $Client=  new ClientModel;
        $data['clients']=$Client->getClient();

        echo view('common/header.php');
        return (view('client/index.php',$data));
    }
    
    public function add()
	{
        $data = $this->request->getPost();
        $message['succes']='';

        if ($data &&  $this->request->getMethod()=='post')
        {
            $ClientModel=  new ClientModel;
            $Clients = new \App\Entities\Client($data);
            if ($ClientModel->save($Clients))
            {
               $session=\config\Services::session();
               $session->setFlashdata('success','le Client a été ajouter avec succée');
               return redirect()->to('/client');
            }
            
        }
           echo view('common/header.php');
           echo view('client/newClient.php');
    }
    
    public function edit($id)
	{
        $client=  new ClientModel;
        $data['client']=$client->getClientByid($id);
        if ($this->request->getMethod()=='post')
        {
            $dataclient = $this->request->getPost();
            if ($client->update($id, $dataclient))
            {
                $session=\config\Services::session();
                $session->setFlashdata('success','le Client a été modifier avec succée');
                return redirect()->to('/client');

            };
        }    
        echo view('common/header.php');
        return (view('client/editClient.php',$data));
	}

	//--------------------------------------------------------------------
    public function delete($id)
	{
        $client=  new ClientModel;
        $client->where('id',$id)->delete();
        $session=\config\Services::session();
        $session->setFlashdata('success','le Client a été supprimer avec succée');
        return redirect()->to('/client');
   
    }
}
