<?php namespace App\Models;

use CodeIgniter\Model;

class FactureModel extends Model
{
    protected $table         = 'facture';
    protected $allowedFields = [
        'client_id', 'amount','sent_at','status','chrono'
    ];
    protected $returnType    = 'App\Entities\Facture';

    public function getFactures()
    {

        return $this->findAll();
    }

    public function getFactureByid($id)
    {

        return $this->asArray()
                    ->where(['chrono'=>$id])
                    ->first() ;
    }


    public function getFactureClient()
    
    {
        $db= \Config\Database::connect();

        $builder=$db->table('facture');
        $builder->select('*');
        $builder->join('client', 'client.id = facture.client_id');
        $query = $builder->get();
        return  $query->getResult('array');

    }

   
}