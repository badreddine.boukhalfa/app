<?php namespace App\Models;

use CodeIgniter\Model;

class ClientModel extends Model
{
    protected $table         = 'client';
    protected $allowedFields = [
        'nom', 'prenom', 'email','entreprise'
    ];
    protected $returnType    = 'App\Entities\Client';

    public function getClient()
    {

        return $this->findAll();
    }

    public function getClientByid($id)
    {

        return $this->asArray()
                    ->where(['id'=>$id])
                    ->first() ;
    }
}